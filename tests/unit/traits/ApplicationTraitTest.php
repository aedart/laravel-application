<?php

use Aedart\Laravel\Application\Interfaces\ApplicationAware;
use Aedart\Laravel\Application\Traits\ApplicationTrait;
use Aedart\Testing\Laravel\Traits\ApplicationInitiatorTrait;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Foundation\Application as LaravelApplication;

/**
 * Class ApplicationTraitTest
 *
 * @coversDefaultClass Aedart\Laravel\Application\Traits\ApplicationTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class ApplicationTraitTest extends \Codeception\TestCase\Test
{
    use ApplicationInitiatorTrait;

    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Helpers
     *****************************************************************************/

    /**
     * Get mock for given trait
     * @return PHPUnit_Framework_MockObject_MockObject|Aedart\Laravel\Application\Interfaces\ApplicationAware
     */
    protected function getTraitMock(){
        $m = $this->getMockForTrait('Aedart\Laravel\Application\Traits\ApplicationTrait');
        return $m;
    }

    /**
     * Get a dummy implementation
     *
     * @return Aedart\Laravel\Application\Interfaces\ApplicationAware
     */
    protected function getDummyImpl(){
        return new DummyApplicationClass();
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::hasDefaultApplication
     * @covers ::getDefaultApplication
     */
    public function getNullAsDefaultApplication(){
        $trait = $this->getTraitMock();

        $this->assertFalse($trait->hasDefaultApplication());
        $this->assertNull($trait->getDefaultApplication());
    }

    /**
     * @test
     * @covers ::getApplication
     * @covers ::hasApplication
     * @covers ::hasDefaultApplication
     * @covers ::setApplication
     * @covers ::isApplicationValid
     * @covers ::getDefaultApplication
     */
    public function getDefaultApplication(){
        $this->startApplication();

        $trait = $this->getTraitMock();

        $this->assertTrue($trait->hasDefaultApplication());
        $this->assertInstanceOf('Illuminate\Contracts\Foundation\Application', $trait->getApplication());
        $this->assertSame($this->getApplication(), $trait->getApplication());

        $this->stopApplication();
    }

    /**
     * @test
     * @covers ::getApplication
     * @covers ::hasApplication
     * @covers ::setApplication
     * @covers ::isApplicationValid
     */
    public function setAndGetApplication(){
        $trait = $this->getTraitMock();

        $app = new LaravelApplication('');

        $trait->setApplication($app);

        $this->assertSame($app, $trait->getApplication());
    }

    /**
     * @test
     * @covers ::getApplication
     * @covers ::hasApplication
     * @covers ::setApplication
     * @covers ::isApplicationValid
     *
     * @expectedException \Aedart\Laravel\Application\Exceptions\InvalidApplicationException
     */
    public function attemptSetInvalidApplication(){
        $dummy = $this->getDummyImpl();

        $app = new LaravelApplication('');

        $dummy->setApplication($app);
    }
}

class DummyApplicationClass implements ApplicationAware{
    use ApplicationTrait;

    public function isApplicationValid(Application $application) {
        return false;
    }
}
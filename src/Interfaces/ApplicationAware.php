<?php  namespace Aedart\Laravel\Application\Interfaces;

use Aedart\Laravel\Application\Exceptions\InvalidApplicationException;
use Illuminate\Contracts\Foundation\Application;

/**
 * Interface Application Aware
 *
 * Components that implement this interface, promise that an application can be specified
 * and obtained again, when it is needed. Furthermore, depending upon implementation, a
 * default application might be available, if none has been specified prior to obtaining
 * it.
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Laravel\Application\Interfaces
 */
interface ApplicationAware {

    /**
     * Set the application instance
     *
     * @param Application $application The application instance to be used by this component
     *
     * @return void
     *
     * @throws InvalidApplicationException If given application is invalid, e.g. something not configured,
     *                                      certain service providers not available, ...etc
     */
    public function setApplication(Application $application);

    /**
     * Get the application instance
     *
     * If no application instance has been set, this method sets
     * and returns a default instance, if any is available
     *
     * @see getDefaultApplication()
     *
     * @return Application|null Application instance or null if none has been specified
     *                          and no default instance is available
     */
    public function getApplication();

    /**
     * Get a default application instance, if any is available
     *
     * @return Application|null Application instance or null if none is available
     */
    public function getDefaultApplication();

    /**
     * Check if an application instance has been set
     *
     * @return bool True if an application instance has been set, false if not
     */
    public function hasApplication();

    /**
     * Check if a default application instance is available
     *
     * @return bool True if a default application instance is available, false if not
     */
    public function hasDefaultApplication();

    /**
     * Check if the given application is valid - e.g. if specific configuration
     * has been set, certain service providers have been registered, ... etc
     *
     * @param Application $application The application instance to be validated
     *
     * @return bool True if the given application is valid, false if not
     */
    public function isApplicationValid(Application $application);

}
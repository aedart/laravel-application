<?php  namespace Aedart\Laravel\Application\Traits;

use Aedart\Laravel\Application\Exceptions\InvalidApplicationException;
use Aedart\Laravel\Detector\ApplicationDetector;
use Illuminate\Contracts\Foundation\Application;

/**
 * Trait Application
 *
 * @see \Aedart\Laravel\Application\Interfaces\ApplicationAware
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Laravel\Application\Traits
 */
trait ApplicationTrait {

    /**
     * The application instance
     *
     * @var \Illuminate\Contracts\Foundation\Application|null
     */
    protected $application = null;

    /**
     * Set the application instance
     *
     * @param Application $application The application instance to be used by this component
     *
     * @return void
     *
     * @throws InvalidApplicationException If given application is invalid, e.g. something not configured,
     *                                      certain service providers not available, ...etc
     */
    public function setApplication(Application $application){
        if(!$this->isApplicationValid($application)){
            throw new InvalidApplicationException(sprintf('The given application instance is not valid'));
        }
        $this->application = $application;
    }

    /**
     * Get the application instance
     *
     * If no application instance has been set, this method sets
     * and returns a default instance, if any is available
     *
     * @see getDefaultApplication()
     *
     * @return Application|null Application instance or null if none has been specified
     *                          and no default instance is available
     */
    public function getApplication(){
        if(!$this->hasApplication() && $this->hasDefaultApplication()){
            $this->setApplication($this->getDefaultApplication());
        }
        return $this->application;
    }

    /**
     * Get a default application instance, if any is available
     *
     * @return Application|null Application instance or null if none is available
     */
    public function getDefaultApplication(){
        $detector = new ApplicationDetector();
        if($detector->isApplicationAvailable()){
            return app();
        }
        return null;
    }

    /**
     * Check if an application instance has been set
     *
     * @return bool True if an application instance has been set, false if not
     */
    public function hasApplication(){
        if(!is_null($this->application)){
            return true;
        }
        return false;
    }

    /**
     * Check if a default application instance is available
     *
     * @return bool True if a default application instance is available, false if not
     */
    public function hasDefaultApplication(){
        if(!is_null($this->getDefaultApplication())){
            return true;
        }
        return false;
    }

    /**
     * Check if the given application is valid - e.g. if specific configuration
     * has been set, certain service providers have been registered, ... etc
     *
     * @param Application $application The application instance to be validated
     *
     * @return bool True if the given application is valid, false if not
     */
    public function isApplicationValid(Application $application){
        // By default, no validation is performed - this should
        // always be overwritten in concrete situations, if specific
        // validation is required, such as settings / configuration
        // check, service providers set... etc
        return true;
    }

}
<?php  namespace Aedart\Laravel\Application\Exceptions; 

/**
 * Class Invalid Application Exception
 *
 * Throw this exception if an invalid application instance has been provided
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Laravel\Application\Exceptions
 */
class InvalidApplicationException extends \InvalidArgumentException{

}
## Laravel-Application ([Deprecated](https://en.wikipedia.org/wiki/Deprecation), use [Aedart/Laravel-Helpers](https://bitbucket.org/aedart/laravel-helpers) instead) ##

Getter and Setter package for a Laravel application instance. It serves as an alternative to the `app()` method, found in the framework's foundation helpers file.

## Contents ##

[TOC]

## When to use this ##

When your components needs to be aware of a Laravel application instance

## How to install ##

For Laravel version 5.0.x

```
#!console

composer require aedart/laravel-application 1.0.*
```

This package uses [composer](https://getcomposer.org/). If you do not know what that is or how it works, I recommend that you read a little about, before attempting to use this package.

## Quick start ##

Provided that you have an interface, e.g. for a command, you can extend the application-aware interface;

```
#!php
<?php
use Aedart\Laravel\Application\Interfaces\ApplicationAware;

interface ICommand extends ApplicationAware {

    // ... Remaining interface implementation not shown ...
    
}

```

In your concrete implementation, you simple use the application-traits;
 
```
#!php
<?php
use Aedart\Laravel\Application\Traits\ApplicationTrait;

class DoSomethingCommand implements ICommand {
 
    use ApplicationTrait;

    // ... Remaining implementation not shown ... 
 
}
```

### Default Application instance ###

The `ApplicationTrait` will by default return the current running Laravel application instance, if any is available. However, if none is available, `null` is returned instead.

To ensure that an application is available, before attempting to use it, please use the `hasApplication()` and `hasDefaultApplication()` methods.

### Default Application Validation ###

By default, the `ApplicationTrait` will always assume that the specified application instance is valid. However, if you need to ensure that specific configuration has been set or
certain service providers are available, or some other kind of application-specific validation, then you can do so, by overriding the `isApplicationValid()` method.

```
#!php
<?php
use Aedart\Laravel\Application\Traits\ApplicationTrait;

class DoSomethingCommand implements ICommand {
 
    use ApplicationTrait;

    public function isApplicationValid(Application $application){
        // In this example, we ensure that the database default driver
        // is sqlite
        $defaultDriver = $application['config']['database.default'];
        if($defaultDriver == 'sqlite'){
            return true;
        }
        
        return false;
    }

    // ... Remaining implementation not shown ... 
 
}
```

## Acknowledgement ##

[Taylor Otwell et al.](http://laravel.com/) for one of the best PHP frameworks ever created.

## License ##

[BSD-3-Clause](http://spdx.org/licenses/BSD-3-Clause), Read the LICENSE file included in this package